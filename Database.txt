DATABASE
	- an organized collection of information or data
	- typically refers to INFORMATION STORED IN A COMUTER SYSTEM but it can also refer to PHYSICAL DATABASES
	- managed by DBMS

DATA	- a raw and does not carry any specific meaning
INFORMATION - a group of  organized data that contains logical meaning

DATABASE MANAGEMENT SYSTEM (DBMS)
	- a system specifically designed to manage the storage, retrieval and modification of data in database
	- allows four types of operations when it comes to handling data : CRUD Operations
	CRUD Operations:
		1. Create (insert)
		2. Read (select)
		3. Update
		4. Delete

RELATIONAL DB - a type of DB where data is stored as a set of tables with rows and columns.
STRUCTURED QUERY LANGUAGE (SQL)
	- the language used typically in relational DBMS to store, retrieve, and modify data

UNSTRUCTURED DATA - data that cannot fit into a strict tabular format
	- commonly used in NoSQL

NOT ONLY SQL (NoSQL)
	- conceptualized when capturing complex, unstructured data became more difficult

MongoDB
	- part of the word "humongous" meaning "huge" or "enormous"
	- open-structure database
	- leading NoSQL database
	- uses key-value pairs
	- language is highly expressive, and generally friendly to those already familiar with the JSON structure
	- stored data using the JSON format
	
	SOME  TERMS IN RELATIONAL DB WILL BE CHANGED:
		1. tables		->	collections
		2. rows		->	documents
		3. columns	->	fields